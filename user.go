package main

import (
	"net/http"
	"regexp"
	"strings"
)

// Regular expressions for matching parts of the request URL.
var (
	zoneRegex = regexp.MustCompile(`zones/([0-9a-zA-Z-.]+)`)
	denyRegex = regexp.MustCompile(`(/config|/search-\w+$|config)`)
)

// User represents a client with zone permissions
type User struct {
	Token string
	Zones map[string]string
}

// RequestAllowed checks if a request is allowed
func (u *User) RequestAllowed(r *http.Request) bool {
	var zone string

	// Always deny access to certain paths
	if denyRegex.MatchString(r.URL.Path) {
		return false
	}

	// Get zone for current request
	matches := zoneRegex.FindStringSubmatch(r.URL.Path)
	if len(matches) >= 2 {
		zone = matches[1]
	}

	// Remove trailing dot
	if len(zone) > 1 && zone[len(zone)-1] == '.' {
		zone = zone[0 : len(zone)-1]
	}

	return u.ZoneAllowed(zone, r.Method)
}

// ZoneAllowed checks if a zone/method combination is allowed
func (u *User) ZoneAllowed(zone, method string) bool {
	perm, ok := u.Zones[zone]
	if !ok {
		return false
	}
	if method == "GET" {
		return strings.ContainsRune(perm, 'r')
	}
	return strings.ContainsRune(perm, 'w')
}
