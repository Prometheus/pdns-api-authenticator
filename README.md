# PowerDNS API Authenticator

Minimal authentication API for use with the PowerDNS API.
Allows certain tokens read and/or write access to configured zones.

**Note**: this is an extremely crude 'authentication' layer,
don't use it unless you know what you're doing.

Download and install with Go:

```
go get git.slxh.eu/prometheus/pdns-api-authenticator
```

Start the program with:

```
pdns-api-authenticator -config config.json
```

Where `config.json` is the path to the configuration file.
The configuration contains the remote API settings as well as user definitions:

```json
{
  "listen": ":8082",
  "server": "pdns-auth:8081",
  "token":  "secret token",
  "users": {
    "user1": {
      "token": "less secret token",
      "zones": {
        "": "r",
        "example.nl": "r",
        "example.com": "rw"
      }
    },
    "user2": {
      "token": "another token",
      "zones": {
         "example.com": "r"
      }
    }
  }
}
```

Access to none-zone endpoints can be configured by setting a value for `""`,
which defaults to `""` (no permissions).
In the example configuration above the permissions are as follows:

- User1 has acess to the zone list and statistics, can view (GET) everything in `example.nl`
  and can read and write to `example.com`.
- User2 cannot list the zones or view the statistics but can view (GET) everything in example.com.

Access to the search and config endpoints is always denied.
