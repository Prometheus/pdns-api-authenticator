package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

// ReloadSignal is the signal for reloading configuration
const ReloadSignal = syscall.SIGUSR1

// Config represents this program's configuration
type Config struct {
	Listen string
	Server string
	Token  string
	Users  map[string]*User
	Tokens map[string]*User
	Mutex  sync.RWMutex
}

// Setup signal
func configSetup(file string) {
	c := make(chan os.Signal)
	signal.Notify(c, ReloadSignal)
	go func() {
		for {
			<-c
			loadConfig(file)
		}
	}()
	loadConfig(file)
}

// loadConfig loads the configuration from disk.
func loadConfig(f string) {
	config.Mutex.Lock()
	defer config.Mutex.Unlock()

	// Open config file
	log.Printf("Loading configuration from %s", f)
	file, err := os.Open(f)
	if err != nil {
		log.Fatal("Unable to open configuration file: ", err)
	}

	// Read json.
	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		log.Print("Error reading configuration: ", err)
		if config.Users == nil {
			os.Exit(1)
		}
		return
	}

	// Map tokens to users
	config.Tokens = make(map[string]*User)
	for _, user := range config.Users {
		config.Tokens[user.Token] = user
	}
}
