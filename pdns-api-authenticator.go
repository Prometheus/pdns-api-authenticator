package main

import (
	"bytes"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// APIKeyHeader is the header used for authentication
const APIKeyHeader = "X-Api-Key"

// config is the parsed configuration
var config Config

// Request handler
func apiHandler(w http.ResponseWriter, r *http.Request) {
	config.Mutex.RLock()
	defer config.Mutex.RUnlock()

	// Get token
	var token string
	if t, ok := r.Header[APIKeyHeader]; ok && len(t) == 1 {
		token = t[0]
	} else {
		token = r.URL.Query().Get("access_token")
	}

	// Check token and forward request
	if user, ok := config.Tokens[token]; ok && user.RequestAllowed(r) {
		forwardRequest(w, r)
	} else {
		http.Error(w, "Unauthorized Request", http.StatusUnauthorized)
	}
}

// Forward request to the PowerDNS API
func forwardRequest(w http.ResponseWriter, r *http.Request) {
	// Copy the body for later use
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Create the URL to the server
	url := config.Server + r.RequestURI

	// Create the new request
	req, _ := http.NewRequest(r.Method, url, bytes.NewReader(body))
	req.Header.Add(APIKeyHeader, config.Token)

	// Perform request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadGateway)
		return
	}

	// Write response
	w.WriteHeader(res.StatusCode)
	io.Copy(w, res.Body)
}

func main() {
	var configFile string
	var listen string

	// Flags
	flag.StringVar(&configFile, "config", "/etc/pdns-api-authenticator.json", "Configuration file")
	flag.StringVar(&listen, "listen", ":8082", "Listen address/port")
	flag.Parse()

	// Load the current configuration and setup the reload signal
	configSetup(configFile)

	// Set the listen from config if set
	if config.Listen != "" {
		listen = config.Listen
	}

	// Setup the API
	http.HandleFunc("/", apiHandler)

	// Start the server
	log.Print("Listening on: ", listen)
	log.Fatal(http.ListenAndServe(config.Listen, nil))
}
